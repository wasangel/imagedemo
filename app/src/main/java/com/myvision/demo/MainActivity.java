package com.myvision.demo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.myvision.demo.Activities.LoginActivity;
import com.myvision.demo.Adapters.ImageAdapter;
import com.myvision.demo.DataModels.ApiResponse;
import com.myvision.demo.DataModels.ImageModel;
import com.myvision.demo.DataModels.ImageResponse;
import com.myvision.demo.Utils.AppController;
import com.myvision.demo.Utils.SessionManager;
import com.myvision.demo.Utils.WebService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.fabLogout)
    FloatingActionButton fabLogout;
    @BindView(R.id.recyclerViewImages)
    RecyclerView recyclerViewImages;
    @BindView(R.id.shimmerImageList)
    ShimmerFrameLayout shimmerImageList;
    @BindView(R.id.progressPagination)
    ProgressBar progressPagination;
    @BindView(R.id.btnLoadMore)
    Button btnLoadMore;
    private SessionManager mySessionManager;
    private Activity activity = MainActivity.this;

    private int pageNumber = 1;
    private int itemCout = 10;
    private ImageAdapter imageAdapter;
    private String apiToken;


    private boolean isLoading = false;
    private int pastVisibleItems, visibleItemCount, totalItemCount, previousTotal = 0;
    private int viewThreshold;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Image Demo");
        mySessionManager = new SessionManager(activity);
        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerViewImages.setLayoutManager(layoutManager);
        recyclerViewImages.setItemAnimator(new DefaultItemAnimator());
        recyclerViewImages.hasFixedSize();
        recyclerViewImages.setItemViewCacheSize(500);
        shimmerImageList.startShimmer();


        mySessionManager = new SessionManager(activity);
        if (mySessionManager.checkLoggedIn()) {

            apiToken = mySessionManager.getSpAuthTokenValue();
        }


        onGetImages();

//        recyclerViewImages.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//
//                visibleItemCount = layoutManager.getChildCount();
//                totalItemCount = layoutManager.getItemCount();
//                pastVisibleItems = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
//
//                if (dy > 0) {
//                    if (isLoading) {
//                        if (totalItemCount > previousTotal) {
//                            isLoading = false;
//                            previousTotal = totalItemCount;
//                        }
//                    }
//
//
//                    if (!isLoading && (totalItemCount - visibleItemCount <= (pastVisibleItems + viewThreshold))) {
//                        pageNumber++;
//                        performPagination();
//                        isLoading = true;
//                    }
//                }
//            }
//        });


    }


    private void onGetImages() {


        StringRequest imageRequest = new StringRequest(Request.Method.GET, WebService.API_READ_IMAGES + pageNumber,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("MY_RESPONSE", "onResponse: " + response);
                        ApiResponse apiResponse = new Gson().fromJson(response, ApiResponse.class);

                        ImageResponse imageResponse = apiResponse.getData();
                        List<ImageModel> imageModelList = imageResponse.getData();

                        recyclerViewImages.setVisibility(View.VISIBLE);

                        Log.d("IMG_LIST", "onResponse: " + imageModelList.size());
                        imageAdapter = new ImageAdapter(activity, imageModelList);
                        recyclerViewImages.setAdapter(imageAdapter);
                        shimmerImageList.stopShimmer();
                        shimmerImageList.setVisibility(View.GONE);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put(WebService.API_PARAM_ACCEPT, WebService.API_PARAM_VALUE_ACCEPT);
                headers.put(WebService.API_PARAM_VALUE_AUTHORIZATION, WebService.API_PARAM_VALUE_AUTHORIZATION_VALUE + apiToken);
                return headers;
            }

        };

        AppController.getInstance().addToRequestQueue(imageRequest);
    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;
    }


    private void performPagination() {
        progressPagination.setVisibility(View.VISIBLE);
        StringRequest imageRequest = new StringRequest(Request.Method.GET, WebService.API_READ_IMAGES + pageNumber,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("MY_RESPONSE", "onResponse: " + response);

                        ApiResponse apiResponse = new Gson().fromJson(response, ApiResponse.class);

                        ImageResponse imageResponse = apiResponse.getData();
                        List<ImageModel> imageModelList = imageResponse.getData();
                        imageAdapter.addMoreImage(imageModelList);
                        progressPagination.setVisibility(View.GONE);

                        if (imageModelList.size() == 0) {
                            Toast.makeText(activity, "No More Items", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put(WebService.API_PARAM_ACCEPT, WebService.API_PARAM_VALUE_ACCEPT);
                headers.put(WebService.API_PARAM_VALUE_AUTHORIZATION, WebService.API_PARAM_VALUE_AUTHORIZATION_VALUE + apiToken);
                return headers;
            }

        };

        AppController.getInstance().addToRequestQueue(imageRequest);
    }


    @OnClick({R.id.fabLogout, R.id.btnLoadMore})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.fabLogout:
                pageNumber++;
                performPagination();

                if (mySessionManager.checkLoggedIn()) {
                    mySessionManager.logout();
                    startActivity(new Intent(activity, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                }
                break;
            case R.id.btnLoadMore:
                pageNumber++;
                performPagination();
                break;
        }
    }
}
