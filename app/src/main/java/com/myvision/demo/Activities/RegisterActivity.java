package com.myvision.demo.Activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.myvision.demo.R;
import com.myvision.demo.Utils.ConstantFunctions;
import com.myvision.demo.Utils.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.etxRegName)
    AppCompatEditText etxRegName;
    @BindView(R.id.etxRegEmail)
    AppCompatEditText etxRegEmail;
    @BindView(R.id.etxRegMobile)
    AppCompatEditText etxRegMobile;
    @BindView(R.id.etxRegPass)
    AppCompatEditText etxRegPass;
    @BindView(R.id.etxRegConfirmPass)
    AppCompatEditText etxRegConfirmPass;
    @BindView(R.id.btnRegSignUp)
    Button btnRegSignUp;
    @BindView(R.id.txtSignIn)
    TextView txtSignIn;
    @BindView(R.id.layoutRegister)
    LinearLayout layoutRegister;

    private Activity activity = RegisterActivity.this;
    private String userName, userEmail, userMobile, userPass, userCpass;
    private SessionManager mySessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);
        setTitle("");

        txtSignIn.setPaintFlags(txtSignIn.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        Animation slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        Animation slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);

        mySessionManager = new SessionManager(activity);
        if (layoutRegister.getVisibility() == View.GONE) {

            layoutRegister.startAnimation(slideUp);
            layoutRegister.setVisibility(View.VISIBLE);
        }
    }

    private boolean onCheckInput() {

        if (TextUtils.isEmpty(userName)) {
            ConstantFunctions.OnShowAlertMessage(activity, "Enter your Name");
            return false;
        } else if (TextUtils.isEmpty(userEmail) || !Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()) {
            ConstantFunctions.OnShowAlertMessage(activity, "Please Enter Valid Email Id");
            return false;
        } else if (TextUtils.isEmpty(userMobile) || !Patterns.PHONE.matcher(userMobile).matches()) {
            ConstantFunctions.OnShowAlertMessage(activity, "Enter Valid Mobile Number");
            return false;
        } else if (TextUtils.isEmpty(userPass)) {
            ConstantFunctions.OnShowAlertMessage(activity, "Enter Your Password");
            return false;
        } else if (TextUtils.isEmpty(userCpass)) {
            ConstantFunctions.OnShowAlertMessage(activity, "Enter Your Confirm Password");
            return false;
        } else if (!userCpass.equals(userPass)) {
            ConstantFunctions.OnShowAlertMessage(activity, "Passwords are Mismatch");
            return false;
        }
        return true;
    }

    @OnClick({R.id.btnRegSignUp, R.id.txtSignIn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnRegSignUp:
                userName = etxRegName.getText().toString().trim();
                userEmail = etxRegEmail.getText().toString().trim();
                userMobile = etxRegMobile.getText().toString().trim();
                userPass = etxRegPass.getText().toString().trim();
                userCpass = etxRegConfirmPass.getText().toString().trim();
                if (onCheckInput()) {

                }
                break;
            case R.id.txtSignIn:
                startActivity(new Intent(activity, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));

                break;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
