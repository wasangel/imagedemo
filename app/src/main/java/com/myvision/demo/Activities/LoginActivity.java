package com.myvision.demo.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.myvision.demo.DataModels.AuthResponseModel;
import com.myvision.demo.MainActivity;
import com.myvision.demo.R;
import com.myvision.demo.Utils.AppController;
import com.myvision.demo.Utils.ConstantFunctions;
import com.myvision.demo.Utils.SessionManager;
import com.myvision.demo.Utils.WebService;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.etxLoginEmail)
    AppCompatEditText etxLoginEmail;
    @BindView(R.id.etxLoginPass)
    AppCompatEditText etxLoginPass;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.txtForgotPass)
    TextView txtForgotPass;
    @BindView(R.id.txtRegister)
    TextView txtRegister;
    @BindView(R.id.layoutLogin)
    LinearLayout layoutLogin;

    private Activity activity = LoginActivity.this;
    private SessionManager mySessionManager;
    private String emailId, userPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        txtForgotPass.setPaintFlags(txtForgotPass.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtRegister.setPaintFlags(txtRegister.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

//        LinearLayout layoutLogin = findViewById(R.id.layoutLogin);


        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);
        setTitle("");

        Animation slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
//        Animation slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);

        mySessionManager = new SessionManager(activity);
        if (mySessionManager.checkLoggedIn()) {
            startActivity(new Intent(activity, MainActivity.class));
            finish();
        }

        if (layoutLogin.getVisibility() == View.GONE) {

            layoutLogin.startAnimation(slideUp);
            layoutLogin.setVisibility(View.VISIBLE);
        }
    }

    @OnClick({R.id.btnLogin, R.id.txtForgotPass, R.id.txtRegister})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                emailId = etxLoginEmail.getText().toString().trim();
                userPass = etxLoginPass.getText().toString().trim();
                if (onCheckInputs()) {
                    ConstantFunctions.hideKeyBoard(activity);
                    onLogin();
                }
                break;
            case R.id.txtForgotPass:
                break;
            case R.id.txtRegister:
                startActivity(new Intent(activity, RegisterActivity.class));

                break;
        }
    }

    private void onLogin() {

        final ProgressDialog progressDialog = ConstantFunctions.onShowProgressDialog(activity);
        StringRequest loginRequest = new StringRequest(Request.Method.POST, WebService.API_AUTH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        progressDialog.dismiss();
                        onCheckResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                Toast.makeText(activity, error.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d("ERROR", "onErrorResponse: " + error.getLocalizedMessage());
                Log.d("ERROR", "onErrorResponse: " + error.getMessage());
                Log.d("ERROR", "onErrorResponse: " + error.toString());
                // onHandleError(error.getMessage());

            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<>();
                params.put(WebService.PARAM_USERNAME, emailId);
                params.put(WebService.PARAM_PASSWORD, userPass);
                params.put(WebService.PARAM_GRANT_TYPE, WebService.PARAM_GRANT_TYPE_VALUE);
                params.put(WebService.PARAM_CLIENT_ID, WebService.PARAM_CLIENT_ID_VALUE);
                params.put(WebService.PARAM_CLIENT_SECRET, WebService.PARAM_CLIENT_SECRET_VALUE);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put(WebService.API_PARAM_ACCEPT, WebService.API_PARAM_VALUE_ACCEPT);
                headers.put(WebService.API_PARAM_VALUE_AUTHORIZATION, WebService.API_PARAM_VALUE_AUTHORIZATION_VALUE);
                return headers;
            }
        };
        AppController.getInstance().addToRequestQueue(loginRequest);
    }

//    private void onHandleError(String message) {
//        ErrorResponseModel errorResponseModel = new Gson().fromJson(message, ErrorResponseModel.class);
//        ConstantFunctions.OnShowAlertMessage(activity, errorResponseModel.getErrorDescription());
//    }

    private void onCheckResponse(String response) {
        AuthResponseModel apiResponse = new Gson().fromJson(response, AuthResponseModel.class);
        mySessionManager.onLoggedIn(apiResponse.getAccessToken());
        startActivity(new Intent(activity, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
        finish();


    }

    private boolean onCheckInputs() {

        if (TextUtils.isEmpty(emailId)) {
            ConstantFunctions.OnShowAlertMessage(activity, "Enter your username");
            return false;
        } else if (TextUtils.isEmpty(userPass)) {
            ConstantFunctions.OnShowAlertMessage(activity, "Enter your Password");
            return false;
        }
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
