package com.myvision.demo.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.myvision.demo.DataModels.ImageModel;
import com.myvision.demo.R;
import com.myvision.demo.Utils.WebService;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.MyViewHolder> {
    private Context context;
    private List<ImageModel> imageList;

    public ImageAdapter(Context context, List<ImageModel> imageList) {
        this.context = context;
        this.imageList = imageList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_image_layout, viewGroup, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        ImageModel imageModel = imageList.get(i);
        Log.d("ADAPTER", "onBindViewHolder: "+imageModel.getName());
        myViewHolder.txtImageName.setText(imageModel.getName());
        Picasso.get().load(WebService.IMAGE_BASE_PATH + imageModel.getPath()).into(myViewHolder.imgItem);
    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgItem;
        TextView txtImageName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgItem = itemView.findViewById(R.id.imgItem);
            txtImageName = itemView.findViewById(R.id.txtImageName);
        }
    }

    public void addMoreImage(List<ImageModel> imgList)
    {
        for (ImageModel img: imgList)
        {
            imageList.add(img);
        }
        notifyDataSetChanged();
    }
}
