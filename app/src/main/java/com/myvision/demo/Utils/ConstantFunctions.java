package com.myvision.demo.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.ActivityCompat;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

import com.myvision.demo.R;

public class ConstantFunctions {
    public static void OnShowAlertMessage(final Context context, String message) {

        AlertDialog.Builder myDialog = new AlertDialog.Builder(context);
        myDialog.setTitle("");
        myDialog.setMessage(message);


        myDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        myDialog.create();
        myDialog.show();


    }

    public static ProgressDialog onShowProgressDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        progressDialog.show();
        progressDialog.setIndeterminate(true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.progresslayout);

        return progressDialog;
    }

    public static void hideKeyBoard(Activity context) {
        InputMethodManager inputManager = (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

//    public static void checkPermission(Activity context) {
//        int PERMISSION_ALL = 100;
//        String[] PERMISSIONS = {
//                Manifest.permission.READ_SMS,
//                Manifest.permission.RECEIVE_SMS,
//        };
//
//        if (!hasPermissions(context, PERMISSIONS)) {
//            ActivityCompat.requestPermissions(context, PERMISSIONS, PERMISSION_ALL);
//        }
//    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


}
