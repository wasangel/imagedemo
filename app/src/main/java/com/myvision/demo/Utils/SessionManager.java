package com.myvision.demo.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManager {

    private static final String SP_MAIN_PROFILE = "SP_MAIN_PROFILE";

    private SharedPreferences.Editor editor;
    private SharedPreferences preferences;
    private Context context;
    private static final String IS_LOGGED_IN = "IS_LOGGED_IN";
    private static final String SP_AUTH_TOKEN_VALUE = "SP_AUTH_VALUE";


    public SessionManager(Context context) {
        this.context = context;
        editor = context.getSharedPreferences(SP_MAIN_PROFILE, Context.MODE_PRIVATE).edit();
        preferences = context.getSharedPreferences(SP_MAIN_PROFILE, Context.MODE_PRIVATE);
        editor.apply();
    }


    public void onLoggedIn(String authTokenValue) {
        editor.putBoolean(IS_LOGGED_IN, true);
        editor.putString(SP_AUTH_TOKEN_VALUE, authTokenValue);
        editor.apply();
    }

    public boolean checkLoggedIn() {
        return preferences.getBoolean(IS_LOGGED_IN, false);
    }


    public void logout() {
        editor.clear();
        editor.apply();
    }

    public String getSpAuthTokenValue() {
        return preferences.getString(SP_AUTH_TOKEN_VALUE, "");
    }
}
