package com.myvision.demo.Utils;

public class WebService {

    public static final String IMAGE_BASE_PATH = "https://wasdemo.000webhostapp.com/assets/";
    private static final String API_BASE_URL = "https://wasdemo.000webhostapp.com";
    public static final String API_READ_IMAGES = API_BASE_URL + "/api/images?page=";
    public static final String API_AUTH = API_BASE_URL + "/oauth/token";
    public static final String API_PARAM_ACCEPT = "Accept";
    public static final String API_PARAM_VALUE_ACCEPT = "application/json";
    public static final String API_PARAM_VALUE_AUTHORIZATION = "Authorization";
    public static final String API_PARAM_VALUE_AUTHORIZATION_VALUE = "Bearer ";

    public static final String PARAM_USERNAME = "username";
    public static final String PARAM_GRANT_TYPE = "grant_type";
    public static final String PARAM_GRANT_TYPE_VALUE = "password";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_CLIENT_ID = "client_id";
    public static final String PARAM_CLIENT_ID_VALUE = "2";
    public static final String PARAM_CLIENT_SECRET = "client_secret";
    public static final String PARAM_CLIENT_SECRET_VALUE = "vWeqSN61OD3RZRs5tL5DU5yhOJAzNBT0tmsdJY9w";
    public static final String API_RESPONSE_SUCCESS_VALUE = "success";
}
